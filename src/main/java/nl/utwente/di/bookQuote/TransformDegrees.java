package nl.utwente.di.bookQuote;

public class TransformDegrees {
    public double convertDegrees(String temp){
        return Double.valueOf(temp) *1.8 +32;
    }
}
