package nl.utwente.di.bookQuote;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
        /*** Tests t h e Quoter */
public class TestTransformDegrees {
    @Test
    public void testBook1 ( ) throws Exception {
        TransformDegrees transformDegrees = new TransformDegrees( ) ;
        double price = transformDegrees.convertDegrees("1");
        Assertions.assertEquals(10.0,price,0.0,"price of book 1");
    }
}